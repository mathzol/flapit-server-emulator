var NUMBER_REGEX, NetworksWithCounters, StarSymbols, base, byte_array, byte_dict, convert_digit, flap_matcher, i, isEmpty, isFullyEmpty, lead_byte_array, lead_byte_dict, lead_matcher, len, letter, make_matcher, matchCounter, matchNumericRating, matchStarRating, numToFlapSymbols, parse, ref, regex_branch, rules, scanNumeral, scanStars, self, set,
  indexOf = [].indexOf || function(item) { for (var i = 0, l = this.length; i < l; i++) { if (i in this && this[i] === item) return i; } return -1; };

self = typeof exports !== 'undefined' ? exports : window;

byte_array = ['blank', 'empty_bubble', 1, 'A', 'B', 'C', 'D', '/', 'E', 'F', 'G', 'H', 'I', 2, 'J', 'K', 'L', 'M', 'N', 5, 'O', 'P', 'Q', 'R', 'S', 3, 'T', 'U', 'V', 'W', 'X', 6, 'Z', 'Y', 'heart', '€', '$', 4, '£', '¥', '+', '?', '!', 'full_bubble', '&', '@', '#', '->', ':', 0, '.', 9, 'half_bubble', 'full_star', 'half_star', 'empty_star', 'blank2', 8, 'percent', 7];

lead_byte_array = ['blank', 'blast', 'like_ru', 7, 'like_fr', 'like_us_en', 'like_en', 'like_ja', 'like_ko', 8, 'like_zh', 'follow_us_es', 'follow_us_pt', 'follow_us_ru', 'follow_us_fr', 9, 'follow_us_en', 'follow_us_it', 'follow_us_de', 'follow_us_jp', 'follow_us_ko', 'follow_us_zh', 'check_in_pt', 'check_in_ru', 'check_in_jp', 'check_in_ko', 'check_in_zh', 'check_in_en', 1, '#', 'twitter', 'zomato', 'youtube', 2, 'instagram', 'google_stats', 'vkontakte', 'foursquare', 'swarm', 3, 'yelp', 'tripadvisor', 'weibo', 'dianping', 'facebook', 4, 'smiley', 'facebook_thumb', 'thanks_es', 'thanks_pt', 'thanks_ru', 5, 'thanks_fr', 'thanks_en', 'thanks_it', 'thanks_de', 'thanks_ja', 6, 'thanks_ko', 'thanks_zh'];

self.lead_byte_array = lead_byte_array;

self.byte_array = byte_array;

numToFlapSymbols = function(val, n_syms, n_leading_zeros) {
  var frac_n_digits, i, int_n_digits, ix, ref, suffix, sval, symbols;
  if (n_syms == null) {
    n_syms = 6;
  }
  if (n_leading_zeros == null) {
    n_leading_zeros = 0;
  }
  sval = val.toString();
  suffix = null;
  if (sval.length > n_syms) {
    if (val < 1000000) {
      val = val / 1000;
      suffix = 'K';
    } else if (val < 1000000000) {
      val = val / 1000000;
      suffix = 'M';
    } else if (val < 1000000000000) {
      val = val / 1000000000;
      suffix = 'B';
    } else {
      throw "Cannot convert '" + val + "' to symbols. Too large.";
    }
    int_n_digits = val.toFixed(0).length;
    if (int_n_digits > n_syms - 1) {
      throw "Cannot represent '" + val + "' with " + n_syms + " symbols!";
    } else if (int_n_digits === n_syms - 1) {
      frac_n_digits = 0;
    } else {
      frac_n_digits = n_syms - 1 - int_n_digits - 1;
    }
    sval = val.toFixed(frac_n_digits);
  }
  symbols = sval.split('').map(function(x) {
    if (x === '.') {
      return x;
    } else {
      return Number(x);
    }
  });
  if (suffix) {
    symbols.push(suffix);
  }
  for (ix = i = 0, ref = n_leading_zeros; 0 <= ref ? i < ref : i > ref; ix = 0 <= ref ? ++i : --i) {
    if (symbols.length < n_syms) {
      symbols.unshift(0);
    } else {
      break;
    }
  }
  while (symbols.length < n_syms) {
    symbols.unshift('blank');
  }
  return symbols;
};

self.numToFlapSymbols = numToFlapSymbols;

convert_digit = function(i) {
  var digits, ind;
  digits = '0123456789';
  ind = digits.indexOf(i);
  if (ind !== -1) {
    return ind;
  } else {
    return i;
  }
};

lead_byte_dict = {
  ':)': 'smiley',
  ':-)': 'smiley',
  thumbsup: 'facebook_thumb',
  '(y)': 'facebook_thumb',
  'bla.st': 'blast',
  thanks: 'thanks_en',
  like: 'like_en',
  like_us: 'like_us_en',
  like_us_de: 'like_us_en',
  like_us_it: 'like_us_en',
  like_us_pt: 'like_us_en',
  follow_us: 'follow_us_en',
  check_in: 'check_in_en',
  check_in_de: 'check_in_en',
  check_in_es: 'check_in_en',
  check_in_fr: 'check_in_en',
  check_in_it: 'check_in_en'
};

self.lead_byte_dict = lead_byte_dict;

(base = String.prototype).trim || (base.trim = function() {
  return this.replace(/^\s+|\s+$/g, "");
});

regex_branch = function(x) {
  if (x.length > 1 && /^[a-z_]+$/.test(x)) {
    return x + "\\s*";
  } else {
    return x.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&") + "\\s*";
  }
};

make_matcher = function(mapping) {
  var compileRegex, regex;
  regex = null;
  compileRegex = function() {
    var k, regex_branches, v;
    regex_branches = [];
    for (k in mapping) {
      v = mapping[k];
      regex_branches.push(regex_branch(k));
    }
    regex_branches.sort(function(x, y) {
      return y.length - x.length;
    });
    return new RegExp("^(" + (regex_branches.join("|")) + ")");
  };
  return function(matched) {
    var match;
    regex || (regex = compileRegex());
    match = matched.match(regex);
    if (match) {
      match = match[0];
      return [mapping[match.trim()], matched.slice(match.length)];
    }
  };
};

lead_byte_array.map(function(curr) {
  return lead_byte_dict[curr] = curr;
});

byte_dict = {
  '_': 'blank',
  '%': 'percent',
  '*': 'full_star',
  'euro': '€',
  'dollar': '$',
  'pound': '£',
  'yen': '¥',
  'arrow': '->'
};

self.byte_dict = byte_dict;

byte_array.map(function(curr) {
  return byte_dict[curr] = curr;
});

ref = 'abcdefghijklmnopqrstuvwxyz';
for (i = 0, len = ref.length; i < len; i++) {
  letter = ref[i];
  byte_dict[letter] = letter.toUpperCase();
}

lead_matcher = make_matcher(lead_byte_dict);

flap_matcher = make_matcher(byte_dict);

NUMBER_REGEX = /^\d+\s*$/;

parse = function(matched) {
  var flap, flap_match, flaps, j, lead, lead_flap, lead_len, lead_match, leading_zeros_match, n_blanks_on_left, ref1, rest, val, x;
  matched = matched.trim().toLowerCase() + " ";
  lead = [];
  lead_match = lead_matcher(matched);
  if (lead_match) {
    lead_flap = lead_match[0], rest = lead_match[1];
    if (typeof lead_flap === 'number' && NUMBER_REGEX.test(matched)) {
      return numToFlapSymbols(parseInt(matched), 7);
    }
    lead.push(lead_flap);
    matched = rest;
  } else {
    lead.push('blank');
  }
  flaps = [];
  while (matched.length) {
    flap_match = flap_matcher(matched);
    if (!flap_match) {
      return null;
    }
    flap = flap_match[0], rest = flap_match[1];
    if (flaps.length === 0 && typeof flap === 'number' && NUMBER_REGEX.test(matched)) {
      leading_zeros_match = matched.match(/^0*/);
      val = parseInt(matched);
      lead_len = leading_zeros_match[0].length;
      if (val === 0) {
        lead_len -= 1;
      }
      flaps = numToFlapSymbols(val, 6, lead_len);
      break;
    }
    matched = rest;
    flaps.push(flap);
  }
  n_blanks_on_left = 6 - flaps.length;
  if (n_blanks_on_left < 0) {
    return null;
  }
  for (x = j = 0, ref1 = n_blanks_on_left; 0 <= ref1 ? j < ref1 : j > ref1; x = 0 <= ref1 ? ++j : --j) {
    lead.push('blank');
  }
  return lead.concat(flaps);
};

self.parse = parse;

self.textToFlapSymbols = function(symbols) {
  var j, len1, ref1, ref2, sym, valid;
  if (symbols instanceof Array) {
    valid = true;
    valid = valid && (symbols.length === 7);
    symbols = symbols.map(convert_digit);
    valid = valid && (ref1 = symbols[0], indexOf.call(lead_byte_array, ref1) >= 0);
    ref2 = symbols.slice(1, 7);
    for (j = 0, len1 = ref2.length; j < len1; j++) {
      sym = ref2[j];
      valid = valid && (indexOf.call(byte_array, sym) >= 0);
    }
    if (valid) {
      return symbols;
    } else {
      return null;
    }
  } else if ((typeof symbols) === 'string') {
    return parse(symbols);
  }
};

isFullyEmpty = function(symbols) {
  var j;
  for (i = j = 1; j <= 6; i = ++j) {
    if (!isEmpty(symbols[i])) {
      return false;
    }
  }
  return true;
};

self.isRestricted = function(symbols) {
  var j, len1, parsed, res, rule;
  parsed = self.textToFlapSymbols(symbols);
  if (!parsed) {
    return null;
  }
  if (isFullyEmpty(symbols)) {
    return false;
  }
  for (j = 0, len1 = rules.length; j < len1; j++) {
    rule = rules[j];
    res = rule(symbols);
    if (res) {
      return res;
    }
  }
  return false;
};

set = function(array) {
  var j, k, len1, res;
  res = {};
  for (j = 0, len1 = array.length; j < len1; j++) {
    k = array[j];
    res[k] = true;
  }
  return res;
};

isEmpty = function(c) {
  return c === 'blank' || c === 'blank2';
};

StarSymbols = set(['full_bubble', 'half_bubble', 'empty_bubble', 'half_star', 'empty_star', 'full_star']);

NetworksWithCounters = set(['facebook', 'twitter', 'instagram', 'weibo', 'youtube', 'vkontakte', 'foursquare', 'swarm', 'yelp', 'dianping', 'tripadvisor', 'zomato']);

scanNumeral = function(symbols, pos) {
  var c;
  while (pos < symbols.length) {
    if (isNaN(parseInt(c = symbols[pos])) && c !== '.' && !isEmpty(c)) {
      break;
    }
    ++pos;
  }
  return pos;
};

scanStars = function(symbols, pos) {
  var c;
  while (pos < symbols.length) {
    if (!((c = symbols[pos]) in StarSymbols || isEmpty(c))) {
      break;
    }
    ++pos;
  }
  return pos;
};

matchNumericRating = function(symbols) {
  var pos;
  pos = scanNumeral(symbols, 1);
  return pos < symbols.length && symbols[pos] === "/" && scanNumeral(symbols, pos + 1) === symbols.length;
};

matchStarRating = function(symbols) {
  return scanStars(symbols, 1) === symbols.length;
};

matchCounter = function(symbols) {
  var pos, ref1;
  pos = scanNumeral(symbols, 1);
  return pos === symbols.length || (pos === symbols.length - 1 && ((ref1 = symbols[pos]) === "M" || ref1 === "K" || ref1 === "B"));
};

self.DEFAULT_WORKING_HOURS = {
  start_time: 1,
  close_time: 24 * 60 - 1,
  days: ~0,
  timezone: 'CET'
};

rules = [
  function(symbols) {
    var ref1;
    if (((ref1 = symbols[0]) === 'tripadvisor' || ref1 === 'zomato' || ref1 === 'yelp') && (matchNumericRating(symbols) || matchStarRating(symbols))) {
      return "Flapit cannot display custom messages that resemble user ratings";
    } else {
      return false;
    }
  }, function(symbols) {
    if (symbols[0] in NetworksWithCounters && matchCounter(symbols)) {
      return "Flapit cannot display custom messages that resemble real counters";
    } else {
      return false;
    }
  }
];
