var net = require('net');
var flapit_parser = require('./flapit_parsers');

var port = 443;
var address = '0.0.0.0';

var server = net.createServer(function (socket) {
    // socket.write('Echo server\r\n');
    // socket.pipe(socket);
});

server.listen(port, address, () => {
    console.log('TCP Server is running on port ' + port + '.');
});

let sockets = [];
let buffer;

server.on('connection', function (sock) {
    console.log('CONNECTED: ' + sock.remoteAddress + ':' + sock.remotePort);
    sockets.push(sock);

    sock.on('data', function (data) {
        console.log('DATA ' + sock.remoteAddress + ': ' + data);
        // Write the data back to all the connected, the client will receive it as data from the server

        sockets.forEach(function (sock, index, array) {
            switch (true) {
                case data.indexOf('FLP1-1520003651') !== -1:
                    buffer = Buffer.alloc(8, 0x00);
                    buffer.writeInt32BE(4, 0);
                    buffer.writeInt32BE(1, 4);

                    sock.write(buffer);

                    buffer = Buffer.alloc(8, 0x00);
                    buffer.writeInt32BE(4, 0);
                    buffer.writeInt32BE(4, 4);

                    sock.write(buffer);
                    break;
                default: // heartbeat valaszra kuldunk gyorsba test miatt egy kiirast, egyebkent percenkent kell heartbeatet kuldeni
                    // facebook write
                    const flapit_symbols = flapit_parser.textToFlapSymbols('facebook 1000');

                    buffer = Buffer.alloc(15, 0x00);
                    buffer.writeInt32BE(11, 0);
                    buffer.writeInt32BE(2, 4);

                    for (let index = 0; index < flapit_symbols.length; index++) {
                        const symbol = flapit_symbols[index];

                        console.log(symbol);

                        if (index < 1) {
                            if (flapit_parser.lead_byte_array.indexOf(symbol) !== -1) {
                                buffer.writeUInt8(flapit_parser.lead_byte_array.indexOf(symbol), 8 + index);
                            }
                        } else {
                            if (flapit_parser.byte_array.indexOf(symbol) !== -1) {
                                buffer.writeUInt8(flapit_parser.byte_array.indexOf(symbol), 8 + index);
                            }
                        }
                    }

                    console.log(buffer);

                    sock.write(buffer);
            }
        });
    });

    // Add a 'close' event handler to this instance of socket
    sock.on('close', function (data) {
        let index = sockets.findIndex(function (o) {
            return o.remoteAddress === sock.remoteAddress && o.remotePort === sock.remotePort;
        });

        if (index !== -1) sockets.splice(index, 1);
        console.log('CLOSED: ' + sock.remoteAddress + ' ' + sock.remotePort);
    });
});
